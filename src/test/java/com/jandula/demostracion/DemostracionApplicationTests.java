package com.jandula.demostracion;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jandula.model.Alumno;
import com.jandula.repository.AlumnoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemostracionApplicationTests {

	@Autowired
	private AlumnoRepository repo;
	
	@Test
	public void contextLoads() {
		Alumno a = new Alumno();
		a.setDni("53594474N");
		a.setNombre("Alejandro");
		
		long x = repo.save(a).getId();
		
		assertTrue(x> 0);
		
		Alumno b = repo.findOne(x);
		
		assertTrue(a.equals(b));
		
	    assertNotNull(repo.findByDni("53594474N"));
	}
	
	@Test
	public void leerRegistro() {
		
	}

}
