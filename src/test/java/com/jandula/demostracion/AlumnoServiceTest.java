package com.jandula.demostracion;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jandula.model.Alumno;
import com.jandula.service.AlumnoServicioImp;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlumnoServiceTest {

	private static final Logger logger = LogManager.getLogger(AlumnoServiceTest.class);
	
	@Autowired
	private AlumnoServicioImp service;
	
	@Test
	public void serviceTest() {
		logger.info("Info");
		Alumno a = new Alumno();
		logger.debug("Debug");
		a.setDni("53594474N");
		logger.trace("Trace");
		a.setNombre("Alejandro");
		logger.warn("Warn");
		long x = service.save(a).getId();
		logger.error("Error");
		assertTrue(x> 0);
		logger.fatal("fatal");
	    assertNotNull(service.findByDni("53594474N").equals(a));
	}
}
