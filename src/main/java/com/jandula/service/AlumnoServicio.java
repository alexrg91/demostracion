/**
 * 
 */
package com.jandula.service;

import java.util.List;

import com.jandula.model.Alumno;

/**
 * @author Alex
 *
 */
public interface AlumnoServicio {

	Alumno save(Alumno a);
	
	Alumno findByDni(String dni);
	
	List<Alumno> findAll();
	
	boolean delete(String dni);
	
	Alumno actualizar(Alumno actualizar);
}
