package com.jandula.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jandula.model.Alumno;
import com.jandula.repository.AlumnoRepository;

@Service
public class AlumnoServicioImp implements AlumnoServicio {
	
	@Autowired
	private AlumnoRepository repo;
	
	@Override
	public Alumno save(Alumno a) {
		try {
			return repo.save(a);
		}catch(Exception e) {
			return null;
		}
		
	}

	@Override
	public Alumno findByDni(String dni) {
		return repo.findByDni(dni);
	}

	@Override
	public List<Alumno> findAll() {
		return (List<Alumno>) repo.findAll();
	}

	@Override
	public boolean delete(String dni) {
		Alumno a = repo.findByDni(dni);
		if(a != null) {
			repo.delete(a.getId());
			return true;
		}
		return false;
	}

	@Override
	public Alumno actualizar(Alumno actualizar) {
		Alumno anterior = repo.findOne(actualizar.getId());
		
		if(anterior != null) {
			return repo.save(actualizar);
		}
		
		return null;
	}

}
