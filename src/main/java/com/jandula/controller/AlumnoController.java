package com.jandula.controller;

import java.util.List;

import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jandula.model.Alumno;
import com.jandula.service.AlumnoServicio;
import com.jandula.utils.Utilidades;

@RestController
@RequestMapping("/Alumnos")
public class AlumnoController {

	@Autowired
	private AlumnoServicio servicio;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Alumno> obtenerAlumnos() {
		return servicio.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Alumno> addAlumno(@RequestBody Alumno a) {
		Alumno valor = null;
		if (!StringUtils.isNullOrEmpty(a.getDni()) && !StringUtils.isNullOrEmpty(a.getNombre())) {
			valor = servicio.save(a);
		}

		return new ResponseEntity<Alumno>(valor, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<?> deleteAlumno(@RequestBody String param) {
		ResponseEntity<String> re = null;
		String dni = Utilidades.borrarCaracteres(param);
		if (Utilidades.isDni(dni)) {
			if (servicio.delete(dni)) {
				re = new ResponseEntity<String>("Alumno borrado", HttpStatus.OK);
			} else {
				re = new ResponseEntity<String>("El alumno no existe", HttpStatus.OK);
			}
		} else {
			re = new ResponseEntity<String>("El DNI recibido es nulo o tiene un formato incorrecto", HttpStatus.OK);

		}

		return re;
	}

	@RequestMapping(method = RequestMethod.PATCH)
	@ResponseBody
	public ResponseEntity<Alumno> updateAlumno(@RequestBody Alumno actualizar) {
		if (actualizar != null && actualizar.getId() > 0) {
			return new ResponseEntity<Alumno>(servicio.actualizar(actualizar), HttpStatus.OK);
		}
		return null;
	}
}
