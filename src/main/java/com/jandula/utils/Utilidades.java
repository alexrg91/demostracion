/**
 * 
 */
package com.jandula.utils;

/**
 * The Class Utilidades.
 *
 * @author Alex
 */
public class Utilidades {

	/**
	 * Constructor vacío para hacer la clase estática.
	 */
	private Utilidades() {
		
	}
	
	
	/**
	 * Comprueba si el String es un DNI válido
	 *
	 * @param dni the dni
	 * @return the string
	 */
	public static boolean isDni(String dni) {
		if(dni == null || dni.length() != 9)
			return false;
		return dni.matches("^[0-9]{8}[A-Z]{1}$");
	}
	
	public static String borrarCaracteres(String dni) {
		
		return dni.replace("\"", "");
	}
}
