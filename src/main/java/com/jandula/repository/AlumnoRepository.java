package com.jandula.repository;


import org.springframework.data.repository.CrudRepository;

import com.jandula.model.Alumno;

public interface AlumnoRepository extends CrudRepository<Alumno, Long>{

	Alumno findByDni(String dni);
	
	Alumno findByDniAndNombre(String dni, String nombre);
	
	// Queryjpa
	
	// Query nativa
	
}
